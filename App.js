import Constants from 'expo-constants';
import * as Notifications from 'expo-notifications';
import React, { useState, useEffect, useRef } from 'react';
import { Text, View, Button, Platform, TouchableOpacity, Clipboard, ScrollView } from 'react-native';
import { Paragraph, Dialog } from 'react-native-paper';

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: true,
    shouldSetBadge: true,
  }),
});

export default function App() {
  const [expoPushToken, setExpoPushToken] = useState('');
  const [notification, setNotification] = useState(false);
  const [needShowToken, showHideToken] = useState(false);
  const notificationListener = useRef();
  const responseListener = useRef();

  const copyToClipboard = (text) => {
    Clipboard.setString(text);
  };

  const hideDialog = () => {
    setNotification(false);
  };

  useEffect(() => {
    registerForPushNotificationsAsync().then((token) => setExpoPushToken(token));

    // This listener is fired whenever a notification is received while the app is foregrounded
    notificationListener.current = Notifications.addNotificationReceivedListener((notification) => {
      setNotification(notification);
    });

    // This listener is fired whenever a user taps on or interacts with a notification (works when app is foregrounded, backgrounded, or killed)
    responseListener.current = Notifications.addNotificationResponseReceivedListener((response) => {
      setNotification(response.notification);
    });

    return () => {
      Notifications.removeNotificationSubscription(notificationListener.current);
      Notifications.removeNotificationSubscription(responseListener.current);
    };
  }, []);

  return (
    <View
      style={{
        marginTop: 80,
        flex: 1,
        alignItems: 'center',
      }}
    >
      {!notification && (
        <Button
          onPress={() => showHideToken(!needShowToken)}
          title={!needShowToken ? 'show token' : 'hide token'}
          color="gray"
          visible={!notification}
        ></Button>
      )}
      <TouchableOpacity onPress={() => copyToClipboard(expoPushToken)}>
        <Text style={{ marginTop: 10 }}>{needShowToken && expoPushToken}</Text>
      </TouchableOpacity>
      <Dialog visible={!!notification} onDismiss={hideDialog}>
        <Dialog.ScrollArea>
          <ScrollView>
            <Dialog.Title style={{ flexWrap: 'wrap' }}>{notification && notification.request.content.title}</Dialog.Title>
            <Dialog.Content style={{ flexWrap: 'wrap', flexDirection: 'row' }}>
              <Paragraph>{notification && notification.request.content.body}</Paragraph>
            </Dialog.Content>
          </ScrollView>
        </Dialog.ScrollArea>
      </Dialog>
      <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
        <Text style={{ color: 'gray', fontSize: 16, fontWeight: 'bold' }}>{!notification && 'Waiting for notifications...'} </Text>
      </View>
    </View>
  );
}

async function registerForPushNotificationsAsync() {
  let token;
  console.log('Constants', Constants);
  if (Constants.isDevice) {
    const { status: existingStatus } = await Notifications.getPermissionsAsync();
    let finalStatus = existingStatus;
    if (existingStatus !== 'granted') {
      const { status } = await Notifications.requestPermissionsAsync();
      finalStatus = status;
    }
    if (finalStatus !== 'granted') {
      alert('Failed to get push token for push notification!');
      return;
    }
    token = (await Notifications.getExpoPushTokenAsync()).data;
    console.log(token);
  } else {
    alert('Must use physical device for Push Notifications');
  }

  if (Platform.OS === 'android') {
    try {
      Notifications.setNotificationChannelAsync('default', {
        name: 'default',
        importance: Notifications.AndroidImportance.MAX,
        vibrationPattern: [0, 250, 250, 250],
        lightColor: '#FF231F7C',
      });
    } catch (err) {
      console.log('err', err);
    }
  }

  return token;
}
